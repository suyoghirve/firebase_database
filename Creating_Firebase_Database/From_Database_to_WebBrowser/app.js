const CafeList = document.querySelector('#cafe-list');  // to get the data on to the web browser

// greate element and render cafe (put in cafe)
function renderCafe(doc){
    let li = document.createElement('li');
    let name = document.createElement('span');
    let city = document.createElement('span');
    
    li.setAttribute('data-id', doc.id)
    name.textContent = doc.data().name;
    city.textContent = doc.data().city
    
    li.appendChild(name);
    li.appendChild(city);

    CafeList.appendChild(li);
}


db.collection('Test_Information').get().then((snapshot)=> {
    snapshot.docs.forEach(doc => {
        //console.log(doc.data())     // to get the actual data of firebase database
        renderCafe(doc);
    })  
    //console.log(snapshot.docs); //(to only get the logs and not the data)
})